//
//  Position+CoreDataProperties.swift
//  PosApp
//
//  Created by Tobias Ednersson on 16/03/16.
//  Copyright © 2016 JRSK. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Position {

    @NSManaged var lat: NSNumber?
    @NSManaged var long: NSNumber?
    @NSManaged var date: NSDate?
    @NSManaged var comment: String?

}
