//
//  AutoSendController.swift
//  PosApp
//
//  Created by Tobias Ednersson on 31/03/16.
//  Copyright © 2016 JRSK. All rights reserved.
//

import UIKit

class AutoSendController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var commentField: UITextField!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var onSwitch: UISwitch!
    
    @IBOutlet weak var helpText: UITextView!
    @IBOutlet weak var daySlider: UISlider!
    
    @IBOutlet weak var hourSlider: UISlider!
    let client = LocationClient()
    let sender = LocationSender()
    
    @IBAction func hourChanged(sender: AnyObject) {
       
       let whole = round(self.hourSlider.value)
        UIView.animateWithDuration(0.01, animations: {
            () -> Void in
                self.hourSlider.value = whole
            
            })
        
        
         self.hourLabel.text = String(format: "%.0f", whole)
        //self.hourLabel.text = "\(self.hourSlider.value)"
        Settings.settings.hourToReport = Int(whole)
    }
    
    
 
    
    
    @IBAction func dayChanged(sender: AnyObject) {
      
        
        
        print("hello hello")
        let whole = round(daySlider.value)
        UIView.animateWithDuration(0.01, animations: {() -> Void in
           self.daySlider.value = whole
            
        })
        self.daysLabel.text = String(format: "%.0f", whole)
        
        let numberOfDays = Int(whole);
        
        let today = NSDate();
        Settings.settings.daysToReport = []
        Settings.settings.daysToReport.append(today)
        
        let day = NSDateComponents()
        day.day = 1;
        
        var nextDate = today
        for _ in 1  ..< numberOfDays  {
        Settings.settings.daysToReport.append(nextDate)
            nextDate = NSCalendar.currentCalendar().dateByAddingComponents(day, toDate: nextDate,options:   NSCalendarOptions(rawValue: UInt(0)))!
    }
        print(String(format: "Number of days: %d", (Settings.settings.daysToReport.count)))
    }
    
    @IBAction func onChanged(sender: AnyObject) {
        if(onSwitch.on) {
            
            if(Settings.settings.daysToReport.count == 0) {
                Settings.settings.daysToReport.append(NSDate())
                self.daySlider.value = Float(Settings.settings.daysToReport.count)
                self.daysLabel.text = String(format:"%d", Settings.settings.daysToReport.count)
            }
            self.client.startSignificantUpdates()
        }
        
        else {
            self.client.stopSignficantUpdates()
        }
        
        daySlider.enabled = onSwitch.on
        hourSlider.enabled = onSwitch.on
           }
    
    
    
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        hourSlider.value = Float(Settings.settings.hourToReport)
        
        
            daySlider.value = Float((Settings.settings.daysToReport.count))
        
        daySlider.enabled = Settings.settings.autoUpdate
        hourSlider.enabled = Settings.settings.autoUpdate
        helpText.hidden = !Settings.settings.viewHelpTexts
        self.daysLabel.text = String(format: "%.0f", daySlider.value)
        self.hourLabel.text = String(format: "%.0f", hourSlider.value)
        

        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        onSwitch.on = Settings.settings.autoUpdate
        self.commentField.userInteractionEnabled = Settings.settings.autoUpdate
        self.commentField.text = Settings.settings.comment
        
        
        if(Settings.settings.daysToReport.count == 0) {
            onSwitch.on = false
        }
        
        else {
            self.daySlider.value = Float((Settings.settings.daysToReport.count))
        }
        }
    
    
    override func viewDidDisappear(animated: Bool) {
        Settings.settings.autoUpdate = self.onSwitch.on
        //Settings.settings.daysToReport = roundf(self)
        ///Settings.settings.daysToReport = Int(daySlider.value)
        //Settings.settings.hourToReport = Int(hourSlider.value)
        super.viewDidDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.commentField.delegate = self
     
        // Do any additional setup after loading the view.
    }

    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.commentField.resignFirstResponder()
        if(self.commentField.text != nil) {
            Settings.settings.comment = self.commentField.text!
        
        }
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if(textField.text != nil) {
            Settings.settings.comment = textField.text!
        }
        
        return true;
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
