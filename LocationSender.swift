//
//  LocationSender.swift
//  PosApp
//
//  Created by Tobias Ednersson on 26/02/16.
//  Copyright © 2016 JRSK. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
public class LocationSender: NSObject {
    internal var sender:ConfirmationViewController? = nil
    
        init(sender:ConfirmationViewController) {
            super.init()
            self.sender = sender
    }
    
    override init() {
        super.init()
    }
    
       
   
    
    
    internal func enquePosition(latitude:Double,longitude:Double,comment:String?,date:NSDate){
       let applicationdelegate =  UIApplication.sharedApplication().delegate as! AppDelegate
        let position = NSEntityDescription.insertNewObjectForEntityForName("Position", inManagedObjectContext: applicationdelegate.managedObjectContext ) as! Position
        
            position.lat = latitude
            position.long = longitude
            //position.lat = location.coordinate.latitude
            //position.long = location.coordinate.longitude
            position.date = date
            position.comment = comment;
            applicationdelegate.saveContext()
    
    }
        
    
    
    
    
    
    func sendPositions(doneAction:(() -> Void)?,
                       erroraction:(() ->Void)?,
                       startAction:(() ->Void)?) ->Void {
        if(startAction != nil) {
            startAction!();
            }
            self._sendP(doneAction, errorAction: erroraction);
           }
    
    
    
        internal func makeQueryPart(p:Position) -> String {
            let settingsPath = NSBundle.mainBundle().pathForResource("Settings", ofType: "plist")
            let settings = NSDictionary(contentsOfFile: settingsPath!)
            var username = Settings.settings.username
            var password = Settings.settings.password
            let lat = p.lat?.doubleValue
            let long = p.long?.doubleValue
            let protocoll = settings!["PROTOCOL_VERSION"]!
            
            
            let infoDict = NSBundle.mainBundle().infoDictionary
            let version = infoDict!["CFBundleShortVersionString"] as! String
            let time = p.date!.timeIntervalSince1970 * 1000
            let os = "I";
            let osVersion = UIDevice.currentDevice().systemVersion
            let OsString = String(format: "os=%@&ov=%@&v=%@",os,osVersion,version)
            var comment = p.comment
            
            username = self.escapeString(username)
            password = self.escapeString(password)
            let adresse = settings!["JRSKURL"]!;
            var queryString = "pr=\(protocoll)&u=\(username)&p=\(password)&t=\(time)&la=\(lat!)&lo=\(long!)"
            if((comment) != nil) {
                
                comment = comment?.stringByReplacingOccurrencesOfString("\n", withString: "")
                
                comment = self.escapeString(comment!)
                queryString += "&c=" + comment!
            }
            
            let addrString = "\(adresse)?\(queryString)&\(OsString)"
            print (addrString);
            
            return addrString;
    }
    
    
    
    
    internal func getPositions() -> [Position] {
        var unsent:[Position] = []
        let request = NSFetchRequest(entityName: "Position");
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let sort = NSSortDescriptor(key: "date", ascending: true)
        request.sortDescriptors = [sort]
        do {
            unsent = try delegate.managedObjectContext.executeFetchRequest(request) as! [Position]
        }
        catch{
        }
        return unsent;
    }
    
    internal func  _sendP(endAction:(() ->Void)?,
                          
                          errorAction:(()->Void)?) -> Void {
        let request = NSFetchRequest(entityName: "Position");
        var unsent:[Position] = [];
        let sort = NSSortDescriptor(key: "date", ascending: true)
        request.sortDescriptors = [sort]
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        do {
            //No more positions
            unsent = try delegate.managedObjectContext.executeFetchRequest(request) as! [Position]
            if(unsent.count == 0) {
                endAction!();
                return;
            }
        }
            
        catch(_) {
        
    }
        let position = 	unsent.removeFirst()
        let addrString = self.makeQueryPart(position);
        let url = NSURL(string: addrString)
        var session:NSURLSession
        
        if(self.sender == nil) {
            
            let delegate = SenderDelegate()
          
            session = NSURLSession(configuration:NSURLSessionConfiguration.backgroundSessionConfigurationWithIdentifier("background_send"),delegate: delegate,delegateQueue: nil)

            
            delegate.successBlock =
                {
                    session.finishTasksAndInvalidate()    
                    let manager = DataManager()
                    manager.removeSentPosition(position)
                    
                    var numberLeft:Int = 0
                    do {
                        try numberLeft = DataManager.countLocations()
                    }
                    catch {
                        print("Error counting positions!")
                    }
                    if(numberLeft > 0) {
                        dispatch_async(dispatch_get_main_queue(), {self._sendP(endAction,errorAction: errorAction);})
                    }
                    
                    
            }
            
                  }
        
        else {
            session = NSURLSession.sharedSession();
        }
        
        let blockOnCompletion = {(data:NSData?,response:NSURLResponse?, error:NSError?) in
            
            if(error != nil) {
            
                if(errorAction != nil) {
                dispatch_async(dispatch_get_main_queue(), errorAction!)
                }
                return;
            }
            let httpresponse = response as! NSHTTPURLResponse;
            
            print("Status code: \(httpresponse.statusCode)");
            if(httpresponse.statusCode != 200) {
                
                if(errorAction != nil) {
                dispatch_async(dispatch_get_main_queue(),errorAction!);
                }
                    return;
            }
            
            let manager = DataManager()
            manager.removeSentPosition(position)
            let remaining = self.getPositions()
            if(remaining.count == 0) {
                if(endAction != nil) {
                    dispatch_async(dispatch_get_main_queue(), endAction!)
                }
                    return
            }
           
            //More positions to send
            dispatch_async(dispatch_get_main_queue(), {self._sendP(endAction,errorAction: errorAction);})
            
        };
        
        var sendTask:NSURLSessionTask
        
        
        if(self.sender != nil) {
            sendTask = session.dataTaskWithURL(url!, completionHandler: blockOnCompletion)
        }
        
        else {
            sendTask = session.dataTaskWithURL(url!)
        }
        
        sendTask.resume();
        
    
    
    }
    
    
    
    
    internal func sendPosition(p:Position,doneAction:() ->Void, errorAction:() -> Void, statusError:() -> Void) ->Void {
            let addrstring = makeQueryPart(p);
        
            let url = NSURL(string: addrstring)
            let session = NSURLSession.sharedSession()
            let request = NSURLRequest(URL: url!)
        
        let handler = {(data:NSData?, response:NSURLResponse?,error:NSError?) ->Void in
            
            if(error != nil) {
                print(error)
                dispatch_async(dispatch_get_main_queue(), errorAction)
                return
            }
            
                
            else {
                let httpresponse = response as! NSHTTPURLResponse
                print(httpresponse.statusCode)
                
                
                //Server responded something else than ok....
                if(!(httpresponse.statusCode == 200)) {
                    
                    print(httpresponse.statusCode)
                    dispatch_async(dispatch_get_main_queue(), errorAction)
                    
                    return
                
                }
                
                let delegate = UIApplication.sharedApplication().delegate as! AppDelegate;
                
                let context = delegate.managedObjectContext;
                do {
                context.deleteObject(p)
                try context.save()
                }
                catch {
                    
                }
                
                
                
            }
        }
        
        let task = session.dataTaskWithRequest(request, completionHandler: handler)
        task.resume()
        //self.notifyUser()
    }
 
    private func notifyUser() ->Void {
       
        let message = NSLocalizedString("Alla positioner har skickats till servern", comment: "sent_all_positions")
        let title = NSLocalizedString("Alla skickade", comment: "sent_all_positions_title")
        let controller = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
        controller.addAction(action)
       
        
        self.sender?.activityIndicator.stopAnimating()
        self.sender?.presentViewController(controller, animated: true, completion: nil)
        
        //del.window?.rootViewController?.presentViewController(controller, animated: true, completion: nil)
        let notification = UILocalNotification()
        notification.alertTitle = title
        notification.alertBody = message
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.fireDate = NSDate(timeIntervalSinceNow: 1)
        notification.timeZone = NSTimeZone.defaultTimeZone()
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    }
    
    
    private func errorReport() ->Void {
        let title = NSLocalizedString("Fel vid uppskickning av position", comment: "location_send_error_title")
        let message = NSLocalizedString("Kunde inte skicka din(a) position(er) kontrollera användarnamn och lösenord. Det kan också bero på att appen för närvarande inte har anslutning till internet, ett nytt försök att skicka kommer att göras nästa gång appen startas", comment: "location_send_error_message")
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let action = UIAlertAction(title: "OK", style: .Default, handler: nil);
        alert.addAction(action)
        
        if(self.sender != nil) {
            sender?.presentViewController(alert, animated: true, completion: nil)
        }
        
        else {
            let del = UIApplication.sharedApplication().delegate as! AppDelegate
            del.window?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
        }
            let notification = UILocalNotification()
        notification.alertBody = message
        notification.alertTitle = title
        notification.timeZone = NSTimeZone.defaultTimeZone()
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.fireDate = NSDate(timeIntervalSinceNow: 1)
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
        
        
        
        
        
        
    }
    
    
    private func escapeString(toEscape:String) ->String {
        var toReturn = toEscape.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
       toReturn = toReturn!.stringByReplacingOccurrencesOfString("/", withString: "%2F")
        toReturn = toReturn!.stringByReplacingOccurrencesOfString("&", withString: "%26")
        toReturn = toReturn!.stringByReplacingOccurrencesOfString("\\", withString: "%5C")
        toReturn = toReturn!.stringByReplacingOccurrencesOfString("+", withString: "%2B")
        
        
        return toReturn!;
    }
    
    
}
