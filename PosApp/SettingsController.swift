//
//  SettingsController.swift
//  PosApp
//
//  Created by Tobias Ednersson on 16/02/16.
//  Copyright © 2016 JRSK. All rights reserved.
//

import UIKit

class SettingsController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var userNameField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
   
    @IBOutlet weak var helpTextField: UITextView!
    
    var verificationurl:String?
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var switchShowHelp: UISwitch!
    @IBOutlet weak var switchShowTimeAsUtc: UISwitch!
   
    @IBOutlet weak var switchShowPassword: UISwitch!
    
    @IBOutlet weak var verifyButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameField.returnKeyType = UIReturnKeyType.Done
        let settingsPath = NSBundle.mainBundle().pathForResource("Settings", ofType: "plist")
        let settings = NSDictionary(contentsOfFile: settingsPath!)
        self.verificationurl = settings!["JRSKURL"]! as? String
        if(Settings.settings.username != "" && Settings.settings.password != "") {
            self.verifyButton.setTitle("Byt användare", forState: .Normal)
        }
        
            
        else {
            self.verifyButton.setTitle("Verifiera", forState: .Normal)
        }
        
        
        // Do any additional setup after loading the view.
    }

    @IBAction func verifyPasswordUserName(sender: AnyObject) {
        
        
        if(!Reachability.isConnectedToNetwork()) {
            
          
            let alert = UIAlertController(title: NSLocalizedString("Ingen anslutning", comment: "no_connection"), message: NSLocalizedString("Du kan inte nå internet just nu, omöjligt att verifiera användaren", comment: "cannot verify user"), preferredStyle: .Alert);
            let action = UIAlertAction(title: "OK", style: .Cancel, handler: nil);
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        
        self.verifyButton.hidden = true
        self.activityIndicator.startAnimating()
        let username = self.userNameField.text!
        let password = self.passwordField.text!
        
        let query = String(format: "?pr=1.0&u=%@&p=%@&tl=1", username,password)
        
        
        
        
        
        let url = NSURL(string: self.verificationurl! + query)
        let session = NSURLSession.sharedSession()
        
        let verificationHandler = {(data:NSData?,response:NSURLResponse?,error:NSError?) -> Void in
        
            if(error != nil) {
                
                print(error)
                return;
            
            }
            
            let urlresponse = response! as! NSHTTPURLResponse
            
            if(urlresponse.statusCode == 200) {
               self.validationSuccess()
                print("Validated")
                
            }
            else if(urlresponse.statusCode == 401) {
                self.validationFailure()
                print("Validation failed")
            }
            else {
                print("Something else went very wrong")
            }
        };
        session.dataTaskWithURL(url!, completionHandler: verificationHandler).resume()
    }
    
    func validationSuccess() -> Void {
        let succ = {
            self.verifyButton.setTitle("Byt användare", forState: .Normal)
            self.verifyButton.hidden = false
            self.activityIndicator.stopAnimating()
            self.passwordField.backgroundColor = UIColor.greenColor()
            self.userNameField.backgroundColor = UIColor.greenColor()
            self.tabBarController?.tabBar.hidden = false
            Settings.settings.username = self.userNameField.text!
            Settings.settings.password = self.passwordField.text!
            self.tabBarController?.tabBar.userInteractionEnabled = true
        }
        
        
        dispatch_async(dispatch_get_main_queue(), succ);
        
    }
    
    
    func validationFailure() -> Void {
        let fail = {
                self.activityIndicator.stopAnimating()
                self.passwordField.backgroundColor = UIColor.redColor()
                self.userNameField.backgroundColor = UIColor.redColor()
                self.verifyButton.hidden = false
            
                let errorMessage = UIAlertController(title:  NSLocalizedString("Verifiering misslyckades", comment: "verification failed"), message:  NSLocalizedString("Felaktigt användarnamn eller lösenord", comment: "wrong username"), preferredStyle: .Alert)
            
            
            
                let ok = UIAlertAction(title: "OK", style: .Default, handler: nil)
                errorMessage.addAction(ok);
                self.presentViewController(errorMessage, animated: true, completion: nil)
                self.tabBarController?.tabBar.hidden = true
                self.tabBarController?.tabBar.userInteractionEnabled = false
            if(Settings.settings.password != "" && Settings.settings.username != "") {
                self.userNameField.text = Settings.settings.username
                self.passwordField.text = Settings.settings.username
                self.verifyButton.setTitle("Byt användare", forState: .Normal)
                self.userNameField.backgroundColor = UIColor.greenColor()
                self.passwordField.backgroundColor = UIColor.greenColor()
                self.tabBarController?.tabBar.hidden = false
                self.tabBarController?.tabBar.userInteractionEnabled = true
            
            }
            
        }
        
        dispatch_async(dispatch_get_main_queue(), fail)
        
    }
    
    
    
    override func viewWillAppear(animated: Bool) {
        self.setupFields()
        super.viewWillAppear(animated)
        
        if(Settings.settings.password == "" || Settings.settings.username == "") {
            self.tabBarController?.tabBar.userInteractionEnabled = false
            self.tabBarController?.tabBar.hidden = true;
            self.passwordField.backgroundColor = UIColor.redColor()
            self.userNameField.backgroundColor = UIColor.redColor()
            self.verifyButton.setTitle("Verifiera", forState: .Normal)
        }
        else {
            self.tabBarController?.tabBar.hidden = false;
            self.passwordField.backgroundColor = UIColor.greenColor()
            self.userNameField.backgroundColor = UIColor.greenColor()
        }
    }
        
    
    
    
    @IBAction func toggleUtc(sender: AnyObject) {
       
        Settings.settings.showTimeAsUtc = switchShowTimeAsUtc.on
    }
       
    @IBAction func toggleHelpTexts(sender: AnyObject) {
        // This sets hidden to false if viewHelptexts is true and vice versa
        Settings.settings.viewHelpTexts = switchShowHelp.on
        helpTextField.hidden = !Settings.settings.viewHelpTexts
    }
    
    @IBAction func togglePassword(sender: AnyObject) {
        
        Settings.settings.viewPassword = switchShowPassword.on
        passwordField.secureTextEntry = !Settings.settings.viewPassword
    }
    
    
  
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    private func setupFields() -> Void {
        self.userNameField.text = Settings.settings.username
        self.passwordField.text = Settings.settings.password
        self.switchShowHelp.on = Settings.settings.viewHelpTexts
        self.switchShowPassword.on = Settings.settings.viewPassword
        self.switchShowTimeAsUtc.on  = Settings.settings.showTimeAsUtc
        self.passwordField.secureTextEntry = !Settings.settings.viewPassword
        self.helpTextField.hidden = !Settings.settings.viewHelpTexts
    
        self.userNameField.delegate = self
        self.passwordField.delegate = self
        
    }
   
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?){
        
        
        if(userNameField.isFirstResponder()) {
            userNameField.text = Settings.settings.username
        }
        else if(passwordField.isFirstResponder()) {
            passwordField.text = Settings.settings.password
        }
        view.endEditing(true)
        super.touchesBegan(touches, withEvent: event)
    }
    
    
    
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        
        //Settings.settings.password = passwordField.text!
        //Settings.settings.username = userNameField.text!
        Settings.settings.viewHelpTexts = switchShowHelp.on
        Settings.settings.viewPassword = switchShowPassword.on
        Settings.settings.showTimeAsUtc = switchShowTimeAsUtc.on

        
        //check if both password and username have been given by user...
       /* if(self.passwordField != nil && self.passwordField.text != ""
            && self.userNameField != nil && self.userNameField.text != "") {
            
            self.tabBarController?.tabBar.userInteractionEnabled = true
            self.tabBarController?.tabBar.hidden = false
        
        } */
        /*else {
            self.tabBarController?.tabBar.userInteractionEnabled = false
            self.tabBarController?.tabBar.hidden = true
        }*/

        return true
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
