//
//  SenderDelegate.swift
//  PosApp
//
//  Created by Tobias Ednersson on 2016-12-23.
//  Copyright © 2016 JRSK. All rights reserved.
//

import UIKit

public class SenderDelegate: NSObject,NSURLSessionDelegate,  NSURLSessionDataDelegate {

    static let STATUS_OK = 200
    var latestStatus:Int = 0
    var successBlock:(() ->Void)? = nil
    
    
    public func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask,
                           didReceiveResponse response: NSURLResponse,
                                              completionHandler: (NSURLSessionResponseDisposition) -> Void){
        let httpResponse = response as! NSHTTPURLResponse
        self.latestStatus = httpResponse.statusCode
        
        completionHandler(NSURLSessionResponseDisposition.Allow)
        
        if(self.latestStatus == 200) {
        if(self.successBlock != nil) {
            dispatch_async(dispatch_get_main_queue(), self.successBlock!)
        }
        }
        print("Latest status is: \(self.latestStatus)")
    }

    public func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        print("Completed request")
        if(error == nil && self.latestStatus == SenderDelegate.STATUS_OK && successBlock != nil) {
            
            print("Sucessfully uploaded position in background")
            
        }
    }
    
}
