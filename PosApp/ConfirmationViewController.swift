
//  ViewController.swift
//  PosApp
//
//  Created by Tobias Ednersson on 11/02/16.
//  Copyright © 2016 JRSK. All rights reserved.
//

import UIKit
import CoreLocation
import SystemConfiguration
class ConfirmationViewController: UIViewController,UITextFieldDelegate,DateDelegate {
    enum CompassLat {case North
        case South }
    
    enum CompassLong {case West
        case East }
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var latitudeButton: UIButton!
    @IBOutlet weak var longitudeButton: UIButton!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var commentLabel: UITextField!
    @IBOutlet weak var degreesLongitude: UITextField!
    @IBOutlet weak var minutesLongitude: UITextField!
    @IBOutlet weak var decimalLongitude: UITextField!
    
    @IBOutlet weak var degreesLatitude: UITextField!
    @IBOutlet weak var minutesLatitude: UITextField!
    
    @IBOutlet weak var decimalLatitude: UITextField!
    var position:CLLocation? = nil
    var comment:String?
    var today:NSDate? = nil
    var latestText:String = ""
    var latest_long:Double?
    var latest_lat:Double?
    var longitudeCompass = CompassLong.West
    var latitudeCompass = CompassLat.North
    
    let formatter = NSDateFormatter()
    
    var sender:LocationSender = LocationSender()
    
    
   
   
    
    @IBAction func latitudeChanged(sender: AnyObject) {
        
        if(latitudeCompass == .North) {
            latitudeCompass = .South
        }
        else {
            latitudeCompass = .North
        }
        self.nameButtons()
    }
    
    
    @IBAction func longitudeChanged(sender: AnyObject) {
        if(longitudeCompass == .East) {
            longitudeCompass = .West
        }
        else {
            longitudeCompass = .East
        }
        self.nameButtons()
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
        if(self.position?.coordinate.longitude < 0) {
           self.longitudeCompass = .West
        }
            
        else {
                self.longitudeCompass = .East
                   }
        
        if(position?.coordinate.latitude > 0) {
            self.latitudeCompass = .North
        }
        
        else {
                self.latitudeCompass = .South
        }
        self.nameButtons()
        self.commentLabel.becomeFirstResponder()
        
    }
    
    
    func dateSet(date:NSDate) {
        self.today = date
    }
    
    func resign() -> Void {
        print("Tap tap");
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
     
        self.degreesLatitude.resignFirstResponder()
        self.minutesLatitude.resignFirstResponder()
        self.decimalLatitude.resignFirstResponder()
        self.degreesLongitude.resignFirstResponder()
        self.minutesLongitude.resignFirstResponder()
        self.decimalLongitude.resignFirstResponder()
        self.commentLabel.resignFirstResponder()
    }
    
        
       override func viewDidLoad() {
        super.viewDidLoad()
        self.view.userInteractionEnabled = true
        self.today = self.position?.timestamp
        self.nameButtons()
        if(self.position?.coordinate.latitude < 0) {
           self.latitudeCompass = .South
            
        }
        else {
            self.latitudeCompass = .North
        }
        
        if(self.position?.coordinate.longitude < 0) {
            self.longitudeCompass = .West
        }
        else {
            self.longitudeCompass = .East
        }
        self.nameButtons()
        

        sender.sender = self
        
        formatter.dateStyle = NSDateFormatterStyle.MediumStyle
        formatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        if(Settings.settings.showTimeAsUtc) {
        
            formatter.timeZone = NSTimeZone(abbreviation: "UTC")
        }
        
        
        let longitude = LocationClient.convertToDegMinSec((position?.coordinate.longitude)!);
        let latitude = LocationClient.convertToDegMinSec((position?.coordinate.latitude)!)
        self.today = position?.timestamp
        
        
        
        self.degreesLongitude.keyboardType = UIKeyboardType.NumberPad
        self.minutesLongitude.keyboardType = UIKeyboardType.NumberPad
        self.decimalLongitude.keyboardType = UIKeyboardType.NumberPad
        self.degreesLatitude.keyboardType = UIKeyboardType.NumberPad
        self.minutesLatitude.keyboardType = UIKeyboardType.NumberPad
        self.decimalLatitude.keyboardType = UIKeyboardType.NumberPad

       
        
        self.degreesLatitude.text = String(format: "%.0f", latitude[0])
        self.minutesLatitude.text =  String(format: "%.0f", floor(latitude[1]))
        self.degreesLongitude.text = String(format: "%.0f", longitude[0])
        self.minutesLongitude.text = String(format: "%.0f", floor(longitude[1]))
        
        let dec_longitude = (longitude[1] - floor(longitude[1]) ) * 1000
        let dec_latitude = (latitude[1] - floor(latitude[1])) * 1000
        
        
        
        
                 self.decimalLatitude.text = String(format: "%03.0f", dec_latitude)
                     self.decimalLongitude.text = String(format: "%03.0f", dec_longitude)
      
                self.setupKeyboards()
             }
    
    
    
        
    func nameButtons() -> Void {
        
        switch(self.longitudeCompass) {
        case .East: self.longitudeButton.setTitle("E:", forState: .Normal)
            break;
        case .West:  self.longitudeButton.setTitle("W", forState: .Normal)
        break;
        }
        switch(self.latitudeCompass) {
        case .North: self.latitudeButton.setTitle("N:", forState: .Normal)
            break
        case .South: self.latitudeButton.setTitle("S:", forState: .Normal)
            break
        }
       
        var datestring = self.formatter.stringFromDate(self.today!)
        
        if(Settings.settings.showTimeAsUtc) {
            datestring += " UTC"
        }
        
        self.dateButton.setTitle(datestring, forState: .Normal)
        
    }
    
    
    
    
    func hideKeyboard(button:UIBarButtonItem) -> Bool {
        self.degreesLatitude.resignFirstResponder()
        self.minutesLatitude.resignFirstResponder()
        self.decimalLatitude.resignFirstResponder()
        self.degreesLongitude.resignFirstResponder()
        self.minutesLongitude.resignFirstResponder()
        self.decimalLongitude.resignFirstResponder()
        return true
        
        
    }
    
    
    
    
    
    
    
    func setupKeyboards() ->Void {
        let doneButtonView = UIToolbar(frame: CGRectMake(0,0,320,50))
        doneButtonView.barStyle = .Black
        let doneButton = UIBarButtonItem()
        doneButton.target = self
        doneButton.action = #selector(self.hideKeyboard(_:))
        doneButton.style = .Plain
        doneButton.title = NSLocalizedString("KLAR", comment: "klar")
        doneButtonView.exclusiveTouch = true
        
        doneButtonView.items = [doneButton]
        self.degreesLongitude.inputAccessoryView = doneButtonView
        self.minutesLongitude.inputAccessoryView = doneButtonView
        self.decimalLongitude.inputAccessoryView = doneButtonView
        self.degreesLatitude.inputAccessoryView = doneButtonView
        self.minutesLatitude.inputAccessoryView = doneButtonView
        self.decimalLatitude.inputAccessoryView = doneButtonView
        self.commentLabel.delegate = self
        self.degreesLongitude.delegate = self
        self.degreesLatitude.delegate = self
        self.minutesLongitude.delegate = self
        self.minutesLatitude.delegate = self
        self.decimalLatitude.delegate = self
        self.decimalLongitude.delegate = self
        
        

        
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    
        let count = textField.text?.characters.count
        let newlength = count! - range.length + string.characters.count
        if(textField == self.commentLabel) {
            return newlength <= 100
        }
        
        
       
    
        
        if(newlength == 0) {
            return true;
        }
        
        if(textField == decimalLatitude || textField == degreesLongitude || textField == decimalLongitude) {
            return newlength < 4
        }
            
                    
            
        else {
        return newlength < 3
        }
        
    }
    func textFieldDidBeginEditing(textField: UITextField) {
                
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        if(textField == commentLabel) {
            return true
        }
        
        
        if(textField == minutesLatitude || textField == minutesLongitude) {
            let res = Double(textField.text!)
            return res < 60
        }

        
        //do not leave empty fields
        // if that textfield is not the comment field which may be left empty
        return textField.text?.characters.count > 0
    }
    
    
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let dateController = segue.destinationViewController as! DateChooseController
        dateController.dateToShow = formatter.dateFromString(self.dateButton.titleLabel!.text!)
        dateController.delegate = self
    }
    
    
    @IBAction func cancel(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func confirm(sender: UIButton) {
       
        
        let doneAction = {if(self.activityIndicator.isAnimating()) {
            self.activityIndicator.stopAnimating();
            self.cancelButton.hidden = false
            self.confirmButton.hidden = false
            let confirmation = UIAlertController(title: NSLocalizedString("Positioner skickade", comment: "position sent"), message: NSLocalizedString("Alla positioner har skickats till servern", comment: "all positions sent"), preferredStyle: .Alert)
            
            let action = UIAlertAction(title: "OK", style: .Default, handler: {(action:UIAlertAction) in
            
                dispatch_async(dispatch_get_main_queue(), {
                   self.dismissViewControllerAnimated(true, completion: nil);
                    })})
                
                
            confirmation.addAction(action)
            self.presentViewController(confirmation, animated: true, completion: nil)
            
            }
        }
        
        let errorAction = {self.activityIndicator.stopAnimating()
            self.confirmButton.hidden = false
            self.cancelButton.hidden = false
            let confirmation = UIAlertController(title: NSLocalizedString("Positionen skickades inte", comment: "positions not sent"), message: NSLocalizedString("Misslyckades att skicka positionerna", comment: "failed to send position"), preferredStyle: .Alert)
            confirmation.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            self.presentViewController(confirmation, animated: true, completion: nil)
            
            
            
            
        };
        let statusError = {self.activityIndicator.stopAnimating()
            self.confirmButton.hidden = false
            self.cancelButton.hidden = false};
        let startAction = {
            self.confirmButton.hidden = true;
            self.cancelButton.hidden = true;
            self.activityIndicator.startAnimating()}
        let degrees_long = Double(self.degreesLongitude.text!)
        let minutes_long = Double(self.minutesLongitude.text!)
        let decimal_long = Double(self.decimalLongitude.text!)
        
    
        print (self.position?.coordinate.longitude)
        
        var longitude = degrees_long! + (minutes_long!/60) + (decimal_long!/1000/60)
        
        
        
        
        let degrees_lat = Double(self.degreesLatitude.text!)
        let minutes_lat = Double(self.minutesLatitude.text!)
        
        let decimal_lat = Double(self.decimalLatitude.text!)
        var latitude = degrees_lat! + minutes_lat! / 60 + (decimal_lat!/1000/60)
        
        if(latitudeCompass == .South) {
            latitude = latitude * -1;
            
        }
        
        if(longitudeCompass == .West) {
            longitude = longitude * -1;
        }
        
        
        
        
       if(latitude > 90 || latitude < -90) {
            let message = NSLocalizedString("Vänligen ange ett korrekt värde för latitud", comment: "wrong_lat")
            alertWrongValue(message)
            return
        }
        
        if(longitude > 180 || longitude < -180) {
            let message = NSLocalizedString("Vänligen ange ett korrekt värde för longitud", comment: "wrong_long")
            alertWrongValue(message)
            return;
        }
        let comment = self.commentLabel.text
        if(latitude == latest_lat && longitude == latest_long) {
        
                let resend_action = {(action:UIAlertAction) -> Void in
                    
                           self.sender.enquePosition(latitude, longitude: longitude, comment: comment,date:
                            self.today!)
                        self.sender.sendPositions(doneAction, erroraction: errorAction, startAction: startAction)
                    
            }
         
                confirmResend(resend_action)
            return
        }
        else {
            latest_long = longitude
            latest_lat = latitude
        }

        self.sender.enquePosition(latitude, longitude: longitude, comment: comment,date: today!)
        if(!Reachability.isConnectedToNetwork()) {
            alertNoNetwork()
            return
        }
        //self.cancelButton.hidden = true
        //self.confirmButton.hidden = true
        self.sender.sendPositions(doneAction, erroraction: errorAction,startAction: startAction)
        
        }
        

    func confirmResend(resendAction:(UIAlertAction) -> Void) {
    
    let title = NSLocalizedString("Verkligen sända positionen", comment: "really_send_title")
    let message = NSLocalizedString("Du skickade nyss den här positionen, vill du verkligen skicka den igen?",comment: "really_send_message")
    let confirm_title = NSLocalizedString("OK", comment: "ok")
    let cancel_title = NSLocalizedString("Avbryt", comment: "cancel")
    
    let controller = UIAlertController(title: confirm_title, message: message, preferredStyle: .Alert)
    controller.addAction(UIAlertAction(title: title, style: .Default, handler: resendAction))
    controller.addAction(UIAlertAction(title: cancel_title, style: .Cancel, handler: nil))
    self.presentViewController(controller, animated: true, completion: nil)
    }




    
    
    
    
    func alertNoNetwork() -> Void {
        
        let title = NSLocalizedString("Ingen nätverksanslutning", comment: "no_network")
        let message = NSLocalizedString("Ingen internetanslutning just nu, ett nytt försök att skicka positionerna kommer att göras nästa gång appen startas", comment: "no_network_message")
        let cancel = UIAlertAction(title: "OK", style: .Default, handler: nil)
        let messagePresenter = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        messagePresenter.addAction(cancel)
        self.presentViewController(messagePresenter, animated: true, completion: nil)
    }
    
    
    func alertWrongValue(reason:String) -> Void {
        
        let title = NSLocalizedString("Felaktigt värde", comment: "bad_value")
        let controller = UIAlertController(title: title , message: reason, preferredStyle: .Alert)
        let cancel = UIAlertAction(title: "OK", style: .Default, handler: nil)
        controller.addAction(cancel)
        self.presentViewController(controller, animated: true, completion: nil)
        
        
    }


}

