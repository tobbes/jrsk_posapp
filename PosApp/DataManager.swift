//
//  DataManager.swift
//  PosApp
//
//  Created by Tobias Ednersson on 16/03/16.
//  Copyright © 2016 JRSK. All rights reserved.
//

import UIKit
import CoreData
class DataManager: NSObject {

    
    /* Inserts a position to be sent into the database */
    func insertLocation(longitude:Double,latitude:Double,date:NSDate,comment:String) ->Void {
        
        
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let position = NSEntityDescription.insertNewObjectForEntityForName("Position", inManagedObjectContext: delegate.managedObjectContext) as! Position
        
        position.lat = latitude
        position.long = longitude
        position.date = date
        position.comment = comment
        delegate.saveContext()
        
    }
    
    
   /* Counts the unsent positions */
    static func countLocations() throws -> Int{
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let countRequest = NSFetchRequest(entityName: "Position")
        let positions  = try delegate.managedObjectContext.executeFetchRequest(countRequest)
        return positions.count
    }
    
    func removeSentPosition(p:Position) {
       
       let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
       let context = delegate.managedObjectContext
       context.deleteObject(p)
        do {
            try context.save()
        }
        catch{
            print("Error saving context")
        }
    }
    
    
}
