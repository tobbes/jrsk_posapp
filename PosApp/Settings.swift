//
//  Settings.swift
//  PosApp
//
//  Created by Tobias Ednersson on 16/02/16.
//  Copyright © 2016 JRSK. All rights reserved.
//  A singelton pattern representing various settings the user can make.
//

import UIKit

class Settings {
    
    private static var _settings:Settings? = nil
    
    /*This property exposed to the rest of the app */
    internal static var settings:Settings {
        get{
            if(_settings == nil) {
                _settings = Settings()
            }
            return _settings!
        }
    }
    
    var username:String = ""
    var password:String = ""
    var viewPassword:Bool = false
    var viewHelpTexts:Bool = false
    var autoUpdate:Bool = false
    var showTimeAsUtc:Bool = false
    var daysToReport: [NSDate] = []
    var hourToReport:Int = 0
    var comment:String = "";
    
    static let Keys = ["USERNAME","PASSWORD","VIEW_PASSWORD","WIEW_HELPTEXTS","AUTOUPDATE","TIME_AS_UTC","REPORT_DAYS","REPORT_HOUR","COMMENT"]
    
    
    
    private init() {
        let defaults = NSUserDefaults.standardUserDefaults()
        let savedSettings = defaults.dictionaryForKey("SETTINGS")
        if(!(savedSettings == nil)) {
            
            self.username = savedSettings![Settings.Keys[0]] as! String;
            self.password = savedSettings![Settings.Keys[1]] as! String;
            self.viewPassword = savedSettings![Settings.Keys[2]] as! Bool
            self.viewHelpTexts = savedSettings![Settings.Keys[3]] as! Bool
            self.autoUpdate = savedSettings![Settings.Keys[4]] as! Bool
            self.showTimeAsUtc = savedSettings![Settings.Keys[5]] as! Bool
            self.daysToReport = (savedSettings![Settings.Keys[6]] as? [NSDate])!
            self.hourToReport = savedSettings![Settings.Keys[7]] as! Int
            
            if((savedSettings![Settings.Keys[8]]) != nil) {
                self.comment = (savedSettings![Settings.Keys[8]] as? String)!
            }
            
            else {
                self.comment = ""
            }
            
            

            
        }
    
    
    
        }
        
    
    internal func saveSettings() {
        var toSave:[String:AnyObject] = [:]
       let defaults = NSUserDefaults.standardUserDefaults()
        toSave = [Settings.Keys[0]:self.username,Settings.Keys[1]:self.password,Settings.Keys[2]:self.viewPassword,Settings.Keys[3]:self.viewHelpTexts,Settings.Keys[4]:self.autoUpdate,Settings.Keys[5]:self.showTimeAsUtc,Settings.Keys[6]:self.daysToReport, Settings.Keys[7] : self.hourToReport]
            toSave[Settings.Keys[8]] = self.comment
        defaults.setObject(toSave, forKey: "SETTINGS")
        defaults.synchronize()
        }
    
    }
    
    
    
    
    
    
    

