                        //
//  LocationClient.swift
//  PosApp
//
//  Created by Tobias Ednersson on 11/02/16.
//  Copyright © 2016 JRSK. All rights reserved.
//

import UIKit
import CoreLocation
class LocationClient: NSObject,CLLocationManagerDelegate {
      var locationManager:CLLocationManager?
    
    override init() {
        self.locationManager = CLLocationManager()
        super.init()
        self.locationManager?.delegate = self
    }
    
    static let LOCATION_UPDATE = "LOCATION_UPDATE"
    static let PERMISSION_CHANGED = "GRANTED_WHEN_IN_USE"
    
  
    
    
    private var _distance:Double = 0.0
    
    
    
    var currentLocation:CLLocation? {
        get{
            if(locationManager == nil) {
                locationManager = CLLocationManager()
            }
            
            return locationManager?.location
        }
    }
    var distance:Double {
        get {
            return _distance
        }
    
        set {
          
            if(newValue > 0) {
                _distance = 0.0
            }
            else {
                _distance = newValue
                
            }
            
        }
    
    }

    
    func startSignificantUpdates() -> Bool {
        locationManager?.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        
        if(CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedAlways) {
            locationManager?.requestAlwaysAuthorization()
        }

        
        if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.AuthorizedAlways){
        
            if #available(iOS 9.0, *) {
                locationManager?.allowsBackgroundLocationUpdates = true
            };
            locationManager?.startMonitoringSignificantLocationChanges()
        
        }
        
        return true
    }
    func stopSignficantUpdates() -> Void {
        locationManager?.stopMonitoringSignificantLocationChanges()
    }
    
    
    func stopStandardUpdates() {
        if(locationManager != nil) {
            locationManager?.stopUpdatingLocation()
        }
    }
    
    
    
    
    static func convertToDegMinSec( decimal:Double) ->[Double] {
        
        var dec = decimal
        dec = abs(dec)
        let degrees = floor(dec)
        let minutes = (dec - degrees) * 60
        return [degrees,minutes]
    }
    
    func startStandardUpdates() ->Bool{
        
        if(!CLLocationManager.locationServicesEnabled())
        {
            return false;
            
        }
        
        
        locationManager!.distanceFilter = 0
        locationManager?.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        

        if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.NotDetermined) {
            locationManager?.requestWhenInUseAuthorization()
        }
        
        else if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.Denied) {
            
            return false;
        }
        else {
            locationManager?.startUpdatingLocation()
        }
        
        
        
        
       
        return true
    }
    
    
    func isAuthorized() ->CLAuthorizationStatus {
        return CLLocationManager.authorizationStatus()
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        
        
        if(Settings.settings.autoUpdate && status == CLAuthorizationStatus.AuthorizedAlways) {
            self.startSignificantUpdates()
        }
        
        else {
            self.stopSignficantUpdates()
        }
        
            let notification = NSNotification(name: LocationClient.PERMISSION_CHANGED, object: nil)
            NSNotificationCenter.defaultCenter().postNotification(notification)
        }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print(error.localizedDescription)
    }
    
   
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
       
        
        let location = locations.last
        let notification = NSNotification(name: LocationClient.LOCATION_UPDATE,object: location)
        NSNotificationCenter.defaultCenter().postNotification(notification)
        print ("Comment");
        print(Settings.settings.comment);

        if(!Settings.settings.autoUpdate) {
            return;
        }
        
        
        let manager = DataManager()
        
                for location in locations {
            
            let receivedDate = location.timestamp
            
            let recievedDateComponents = NSCalendar.currentCalendar().componentsInTimeZone(NSTimeZone.defaultTimeZone(), fromDate: receivedDate)
            
            var index = 0
            for dateToReport in Settings.settings.daysToReport {
                let reportComponents = NSCalendar.currentCalendar().componentsInTimeZone(NSTimeZone.defaultTimeZone(), fromDate: dateToReport)
                
                if(reportComponents.year == recievedDateComponents.year &&
                    reportComponents.month == recievedDateComponents.month &&
                    reportComponents.day == recievedDateComponents.day &&
                    reportComponents.hour == Settings.settings.hourToReport) {
                   
                    Settings.settings.daysToReport.removeAtIndex(index)
                    manager.insertLocation(location.coordinate.longitude, latitude: location.coordinate.latitude, date: location.timestamp, comment: Settings.settings.comment)
                    continue
                }
                
                 index += 1
                
                
            
            
            }
            
            
            for reportDate in Settings.settings.daysToReport {
                
                var index = 0;
                if(reportDate.compare((locations.first?.timestamp)!) == .OrderedAscending) {
                    Settings.settings.daysToReport.removeAtIndex(index)
                }
                else {
                    index += 1
                }
            }
            
            
        }
        
        
        if(Settings.settings.daysToReport.count == 0) {
            Settings.settings.autoUpdate = false
        }
        print("received location!!");
    }
    
}
