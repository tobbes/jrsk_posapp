//
//  PositionCell.swift
//  PosApp
//
//  Created by Tobias Ednersson on 21/04/16.
//  Copyright © 2016 JRSK. All rights reserved.
//

import UIKit

class PositionCell: UITableViewCell {

    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
