//
//  DateChooseController.swift
//  PosApp
//
//  Created by Tobias Ednersson on 17/04/16.
//  Copyright © 2016 JRSK. All rights reserved.
//

import UIKit

protocol DateDelegate {
   func dateSet(date:NSDate);
}


class DateChooseController: UIViewController {

   
    @IBAction func dateChanged(sender: AnyObject) {
        self.dateLabel.text = DateFormatter.stringFromDate(datePicker.date)
    }
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var helpText: UITextView!
    let DateFormatter = NSDateFormatter();
    internal var delegate:DateDelegate? = nil
    
    
   internal var dateToShow:NSDate? = nil
    override func viewDidLoad() {
        super.viewDidLoad()

        if(self.dateToShow != nil) {
            self.datePicker.date  = dateToShow!
        }
        
        if(Settings.settings.showTimeAsUtc) {
            DateFormatter.timeZone = NSTimeZone(name: "UTC")
            self.datePicker.timeZone = NSTimeZone(name: "UTC")
    }
        
        else {
                self.DateFormatter.timeZone = NSTimeZone.defaultTimeZone()
                self.datePicker.timeZone = NSTimeZone.defaultTimeZone()
        }
     
        self.DateFormatter.dateStyle = .MediumStyle
        self.DateFormatter.timeStyle = .ShortStyle
        self.dateLabel.text = DateFormatter.stringFromDate(self.datePicker.date)
        
        
            // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
        
  
        
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func okClicked(sender: AnyObject) {
        
        delegate?.dateSet(datePicker.date)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func cancelClicked(sender: AnyObject) {
    
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    

}
