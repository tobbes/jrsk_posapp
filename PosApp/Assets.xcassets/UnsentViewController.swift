//
//  UnsentViewController.swift
//  PosApp
//
//  Created by Tobias Ednersson on 26/02/16.
//  Copyright © 2016 JRSK. All rights reserved.
//

import UIKit
import CoreData
class UnsentViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate {

   
    var dbcontroller:NSFetchedResultsController? = nil
    @IBOutlet weak var helpText: UITextView!
    @IBOutlet weak var positionsTable: UITableView!
    let formatter = NSDateFormatter()
  
    
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
       positionsTable.reloadData()
        
    
    }
    
    private func initDBController() ->Void {
        let request = NSFetchRequest(entityName: "Position");
        let sort = NSSortDescriptor(key: "date", ascending: false)
        request.sortDescriptors = [sort]
        
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.dbcontroller = NSFetchedResultsController(fetchRequest: request, managedObjectContext: delegate.managedObjectContext, sectionNameKeyPath: "date", cacheName: nil)
        
        self.dbcontroller?.delegate = self
        
        return
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
         }
    
    
    override func viewWillAppear(animated: Bool) {
         self.helpText.hidden = !Settings.settings.viewHelpTexts
         positionsTable.reloadData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.formatter.dateStyle = NSDateFormatterStyle.MediumStyle
        self.formatter.timeStyle = NSDateFormatterStyle.MediumStyle
        self.positionsTable.dataSource = self
        self.initDBController()
        
        do {
        try self.dbcontroller!.performFetch()
        }
        catch {
            fatalError("\(error)")
        }
        // Do any additional setup after loading the view.
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("positionRow") as! PositionCell
       
        
        
        let position = self.dbcontroller!.objectAtIndexPath(indexPath) as! Position
        
        if(position.comment != nil) {
            print(position.comment);
        }
        
        
        if(position.date != nil) {
            formatter.dateStyle = NSDateFormatterStyle.MediumStyle
            formatter.timeStyle = NSDateFormatterStyle.MediumStyle
            
            
            if(Settings.settings.showTimeAsUtc) {
                formatter.timeZone = NSTimeZone(abbreviation: "UTC")
                //cell.dateLabel.text = formatter.stringFromDate(position.date!) + " UTC"
                
                cell.dateLabel.text = formatter.stringFromDate(position.date!) + " UTC"
            }
        
            
            
            else {
                cell.dateLabel.text = formatter.stringFromDate(position.date!)
            }
            }
        else {
            cell.dateLabel.text = ""
        }
        
        let longitude = LocationClient.convertToDegMinSec(position.long!.doubleValue)
        let latitude = LocationClient.convertToDegMinSec(position.lat!.doubleValue)
        
        var north_south = "N"
        var east_west = "W"
        
        if(position.lat?.doubleValue < 0) {
            north_south = "S"
        }
    
        if(position.long?.doubleValue < 0) {
            east_west = "E"
        }
        
        
        var message =  String(format:"%@: %.0f° %.3f' %@:%01.0f° %.3f'",north_south,latitude[0],latitude[1],east_west, longitude[0],longitude[1])
        
        
        if(position.comment != nil) {
            
            message += "\n" + position.comment!
        }
        
        
        
        cell.positionLabel.text = message
        
        cell.dateLabel.text = formatter.stringFromDate(position.date!)
        
        if(Settings.settings.showTimeAsUtc) {
            cell.dateLabel.text! += " UTC"
        }
        
        
        
        
       // cell.LongLabel.text = String(format: "Long: %.0f'%.3f", arguments: [longitude[0],longitude[1]])
        //cell.LatLabel.text = String(format: "Lat: %.0f'%.3f", arguments:[latitude[0],latitude[1]])
        return cell
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectioninfo = self.dbcontroller!.sections?[section]
        return (sectioninfo?.numberOfObjects)!
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.dbcontroller!.sections!.count
        }
    
   
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
