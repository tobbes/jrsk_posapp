//
//  AboutControllerViewController.swift
//  PosApp
//
//  Created by Tobias Ednersson on 16/02/16.
//  Copyright © 2016 JRSK. All rights reserved.
//

import UIKit

class AboutControllerViewController: UIViewController {
    
    @IBOutlet weak var versionLabel: UILabel!
    var ICON8URL:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpLink()
        
                // Do any additional setup after loading the view.
        
        self.setVersion();
    
    
    
    }

    func setVersion() -> Void {
        let mainbundle = NSBundle.mainBundle()
        let infodict = mainbundle.infoDictionary
        let vers = infodict!["CFBundleShortVersionString"] as! String
        self.versionLabel.text = "Version \(vers)"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func viewIcon8(sender: AnyObject) {
   let url = NSURL(string: self.ICON8URL)
   
        if(!UIApplication.sharedApplication().canOpenURL(url!)) {
            
            cannotOpenLink()
        }
        
        else {
        UIApplication.sharedApplication().openURL(url!)
        }
        }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func cannotOpenLink() ->Void {
        
       let ac = UIAlertController(title: NSLocalizedString("Url failed", comment: "url_failed_dialogue_title"), message: NSLocalizedString("Could not open the url", comment: "url_failed_dialogue_text"), preferredStyle: UIAlertControllerStyle.Alert)
        
        let dismissblock = { (action:UIAlertAction) -> Void in
        ac.dismissViewControllerAnimated(true, completion: nil)
        }
        
        let cancel = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: dismissblock)
        ac.addAction(cancel)
        
        self.presentViewController(ac, animated: true, completion: nil)
        
        
    }
    
    private func setUpLink() ->Void {
        
        let urllistPath = NSBundle.mainBundle().pathForResource("Settings", ofType: "plist")
        
        if(urllistPath == nil) {
            
                reportError(NSLocalizedString("Could not find settingsfile, app will not function", comment: "plist_missing"))
                return
        }
        
        let urls = NSDictionary(contentsOfFile:urllistPath!)
       
        
        
        if(urls!["ICONS8"] == nil) {
            
            reportError(NSLocalizedString("Settingsfile is corrupt, the app will not function", comment: "settings_corrupt"))
            return
        }
        
        
        self.ICON8URL = urls!["ICONS8"] as! String
        
        
    }

    private func reportError(message:String) ->Void {
        
        let error = NSLocalizedString("There was an error", comment: "general error")
        
     
        let dismiss = {(action:UIAlertAction) -> Void in
        
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
        let cancel = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: dismiss)
        
         let controller = UIAlertController(title: error, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        controller.addAction(cancel)
        self.presentViewController(controller, animated: true, completion: nil)
        
    }
    
}
