
//
//  SendPosControllerViewController.swift
//  PosApp
//
//  Created by Tobias Ednersson on 17/02/16.
//  Copyright © 2016 JRSK. All rights reserved.
//

import UIKit
import CoreLocation

class SendPosControllerViewController: UIViewController,UITextFieldDelegate {
  
    @IBOutlet weak var posQualityLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    let dbmanager:DataManager = DataManager()
    
    let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
    static let MAX_LOCATION_AGE_SECONDS = 60 * 2.0
    @IBOutlet weak var dateLabel: UILabel!
   
    @IBOutlet weak var helpText: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        }
    
    
   
    override func viewWillAppear(animated: Bool) {
        self.helpText.hidden = !Settings.settings.viewHelpTexts
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SendPosControllerViewController.updateLocation(_:)), name:LocationClient.LOCATION_UPDATE , object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SendPosControllerViewController.permissionChanged(_:)), name: LocationClient.PERMISSION_CHANGED, object: nil)
        super.viewWillAppear(animated)
        

        
        
        
    }

    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if(!self.delegate.locclient!.startStandardUpdates()) {
            //  userReport()
            let message = NSLocalizedString("Appen måste få tillgång till din position", comment: "access_location_warning")
            self.positionLabel.text = message
            self.positionLabel.textColor = UIColor.redColor()
            //self.posQualityLabel.text = message
            return;
        }
        if(delegate.locclient?.currentLocation != nil) {
            self.setLocation((delegate.locclient?.currentLocation)!)
        }
    }
    
    
    
    
    func permissionChanged(notification:NSNotification) {
        let status = delegate.locclient!.isAuthorized()
        if(status == CLAuthorizationStatus.Denied) {
            let message = NSLocalizedString("Appen måste få tillgång till din position", comment: "access_location_warning")
            self.positionLabel.text = message
            self.positionLabel.textColor = UIColor.redColor()
            
            delegate.locclient?.stopStandardUpdates()
        }
        else if(status == CLAuthorizationStatus.AuthorizedAlways || status == CLAuthorizationStatus.AuthorizedWhenInUse) {
            delegate.locclient?.startStandardUpdates()
        }
        
        
    }
    
    
    
    func userReport() -> Void {
        
        let alertController = UIAlertController()
        alertController.message = NSLocalizedString("Du måste ge appen åtkomst till din position", comment: "location_unavailable")
        alertController.title = NSLocalizedString("Kunde inte fastställa din position", comment: "location_unavailable_title")
        
        
        let b = {(action:UIAlertAction) -> Void  in
                alertController.dismissViewControllerAnimated(true, completion: nil)
        }
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: b)
        alertController.addAction(action)
        alertController.view.center = self.view.center
        self.presentViewController(alertController, animated: true, completion: nil)
        
        
    }
    
    
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
       self.delegate.locclient?.stopStandardUpdates()
        
        super.viewWillDisappear(animated)
    }
    
    
    
    
    @IBAction func sendPosition(sender: UIButton) {
        if(delegate.locclient?.currentLocation != nil) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let confirmation = storyboard.instantiateViewControllerWithIdentifier("ConfirmationDialogue") as! ConfirmationViewController
            confirmation.position = delegate.locclient!.currentLocation
            
        self.presentViewController(confirmation, animated: true, completion: nil)

            
        }
        
        
        
        
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        segue.destinationViewController
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    func setLocation(location:CLLocation) {
        let today = NSDate()
       if(location.timestamp.timeIntervalSinceDate(today) < -SendPosControllerViewController.MAX_LOCATION_AGE_SECONDS) {
            self.posQualityLabel.textColor = UIColor.redColor()
            self.posQualityLabel.text = NSLocalizedString("Positionen är mer än 2 minuter", comment: "old_pos")
            self.positionLabel.textColor = UIColor.redColor()
        }
        
        
        else {
            let accuracy = (location.horizontalAccuracy + location.verticalAccuracy) / 2
            self.setQuality(accuracy)
        }
        
        
        
        
        
        
        var north_south:String = "N"
        
        if(location.coordinate.latitude < 0) {
            north_south = "S"
            
        }
        
        var east_west = "E"
        
        if(location.coordinate.longitude < 0) {
            east_west = "W"
        }
        
        
        let longitude = LocationClient.convertToDegMinSec(location.coordinate.longitude)
        let latitude =  LocationClient.convertToDegMinSec(location.coordinate.latitude)
        
        let report = String(format:"%@: %.0f° %.3f'\n%@:%01.0f° %.3f'", arguments: [north_south, latitude[0],latitude[1],east_west,longitude[0],longitude[1]])
        
        positionLabel.text = report
        
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.ShortStyle
        formatter.timeStyle = .ShortStyle
        
        if(Settings.settings.showTimeAsUtc) {
            formatter.timeZone = NSTimeZone(abbreviation: "UTC")
        }
        
        var message = formatter.stringFromDate(location.timestamp)
        
        
        if(Settings.settings.showTimeAsUtc) {
            message = "\(message) UTC"
        }
        self.dateLabel.text = message
    }
    
    
    
    
    
    func updateLocation(notif:NSNotification){
             let location = notif.object as! CLLocation
             self.setLocation(location)
    }
    
    func setQuality(accuracy:Double) -> Void {
        
        if(accuracy > 300.0) {
            
            self.posQualityLabel.text = NSLocalizedString("Troligt positionsfel är större än 300 m", comment: "bad_position")
            self.positionLabel.textColor = UIColor.redColor()
            self.posQualityLabel.textColor = UIColor.redColor()
        }
            
        else if(accuracy > 100.0) {
            positionLabel.textColor = UIColor.blueColor()
            self.posQualityLabel.textColor = UIColor.blueColor()
            self.posQualityLabel.text = NSLocalizedString("Troligt positionsfel är större än 100 m  ", comment: "reasonable_position")
        }
        else if(accuracy < 50.0) {
            self.positionLabel.textColor = UIColor.blackColor()
            posQualityLabel.textColor = UIColor.blackColor()
            self.posQualityLabel.text = NSLocalizedString("Troligt positionsfel är mindre än 50 m  ", comment: "good_position")
        }
        else if(accuracy < 100.0) {
            posQualityLabel.textColor = UIColor.blueColor()
            self.positionLabel.textColor = UIColor.blueColor()
            self.posQualityLabel.text = NSLocalizedString("Troligt positionsfel är mindre än 100 m", comment: "fair_position")
                  }
    }
    
    
    
}
